﻿using System;
using System.Diagnostics;

internal class Program
{
    private static void Main(string[] args)
    {
        int[] array1 = new int[100000];
        int[] array2 = new int[1000000];
        int[] array3 = new int[10000000];
        Random random = new Random();
        Program program = new Program();
        for (int i = 0; i < array1.Length; i++)
        {
            array1[i] = 1;
        }

        for (int i = 0; i < array2.Length; i++)
        {
            array2[i] = 1;
        }

        for (int i = 0; i < array3.Length; i++)
        {
            array3[i] = 1;
        }

        Console.WriteLine("100000:");
        Console.WriteLine("-------------------");
        program.Execute(array1);
        Console.WriteLine("-------------------");
        Console.WriteLine("1000000:");
        program.Execute(array2);
        Console.WriteLine("-------------------");
        Console.WriteLine("10000000:");
        program.Execute(array3);
        Console.WriteLine("-------------------");
    }

    public void Execute(int[] array)
    {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        Console.WriteLine($"SimpleSum: {SimpleSum(array)} Time: " + stopwatch.ElapsedMilliseconds);
        stopwatch.Stop();
        stopwatch.Restart();
        Console.WriteLine($"ParallelLinqSum: {ParallelLinqSum(array)} Time: " + stopwatch.ElapsedMilliseconds);
        stopwatch.Stop();
        stopwatch.Restart();
        Console.WriteLine($"ParallelThreadSum: {ParallelThreadSum(array)} Time: " + stopwatch.ElapsedMilliseconds);
        stopwatch.Stop();
    }

    public int SimpleSum(int[] array)
    {
        int sum = 0;

        foreach (int num in array)
        {
            sum += num;
        }

        return sum;
    }

    public int ParallelLinqSum(int[] array)
    {
        return array.AsParallel().Sum();
    }

    public int ParallelThreadSum(int[] array)
    {
        int sum = 0;
        object lockObj = new object();

        List<Thread> threads = new List<Thread>();

        int numThreads = Environment.ProcessorCount;

        int chunkSize = array.Length / numThreads;

        for (int i = 0; i < numThreads; i++)
        {
            int start = i * chunkSize;
            int end = (i == numThreads - 1) ? array.Length : (i + 1) * chunkSize;

            Thread thread = new Thread(() =>
            {
                int localSum = 0;

                for (int j = start; j < end; j++)
                {
                    localSum += array[j];
                }

                lock (lockObj)
                {
                    sum += localSum;
                }
            });

            threads.Add(thread);
            thread.Start();
        }

        foreach (Thread thread in threads)
        {
            thread.Join();
        }

        return sum;
    }
}